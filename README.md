# kanjidep

Kanji dependency information

## Updating

Delete any existing data:
```sh
$ rm -rf tmp data
```

Run `update.rb`:
```sh
$ ruby update.rb
```

# License

This project redistributes the [`CHISE/ids`] project. A copy of the project's
license and acknowledgement is included below.

> **License**
>
> https://gitlab.chise.org/CHISE/ids
> This package is free software; you can redistribute it and/or modify
> it under the terms of the GNU General Public License as published by
> the Free Software Foundation; either version 2, or (at your option)
> any later version.
>
> This package is distributed in the hope that it will be useful, but
> WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
> General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this package; see the file COPYING.  If not, write to
> the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
> Boston, MA 02111-1307, USA.
>
> **Acknowledgements**
>
> The developing of the package was supported by the “Exploratory
> Software Project” of Information-technology Promotion Agency, Japan.
> Some data in the IDS-UCS* files are derived and expanded from the CDP
> database developed by C.C. Hsieh and his team at Academia Sinica in
> Taipei, Taiwan.

This project is distributed under the GNU General Public License, version 3.

See `LICENSE` for more information.


[`CHISE/ids`]: https://gitlab.chise.org/CHISE/ids
