# frozen_string_literal: true

require 'fileutils'
require 'net/http'
require 'set'
require 'json'

require 'rubygems'
require 'zip'

CHISE_IDS_URL = 'https://github.com/chise/ids/archive/refs/heads/master.zip'
CHISE_IDS_FILES = [
  'IDS-UCS-Basic.txt',
  #'IDS-UCS-Compat-Supplement.txt',
  #'IDS-UCS-Compat.txt',
  'IDS-UCS-Ext-A.txt',
  #'IDS-UCS-Ext-B-1.txt',
  #'IDS-UCS-Ext-B-2.txt',
  #'IDS-UCS-Ext-B-3.txt',
  #'IDS-UCS-Ext-B-4.txt',
  #'IDS-UCS-Ext-B-5.txt',
  #'IDS-UCS-Ext-B-6.txt',
  #'IDS-UCS-Ext-C.txt',
  #'IDS-UCS-Ext-D.txt',
  #'IDS-UCS-Ext-E.txt',
  #'IDS-UCS-Ext-F.txt',
  #'IDS-UCS-Ext-G.txt',
  #'IDS-UCS-Ext-H.txt'
]

def update
  download
  build
  puts 'Done!'
end

def download
  return if File.directory?('tmp')

  puts 'Downloading CHISE/ids ...'

  FileUtils.mkdir_p('tmp')

  response = Net::HTTP.get_response(URI(CHISE_IDS_URL))
  response = Net::HTTP.get_response(URI(response['location']))
  File.write('tmp/chiseids.zip', response.body)

  unzip_file('tmp/chiseids.zip', 'tmp')
end

def unzip_file(src, dest)
  Zip::File.open(src) do |zip|
    zip.each do |f|
      path = File.join(dest, f.name)
      FileUtils.mkdir_p(File.dirname(path))
      zip.extract(f, path) unless File.exist?(path)
    end
  end
end

def build
  return if File.directory?('data')

  puts 'Building kanjidep ...'

  FileUtils.mkdir_p('data/kanji')

  entries = get_entries
  parse_ids(entries)
  populate_all_dependencies(entries)
  traverse_all_dependencies(entries)
  populate_all_dependents(entries)

  save_entries(entries)
end

def get_entries
  entries = {}

  CHISE_IDS_FILES.each do |filename|
    path = "tmp/ids-master/#{filename}"
    File.foreach(path) do |line|
      line = line.strip
      next if line.start_with?(';;')
      codepoint, character, ids = line.split("\t")
      entries[character] = {
        'kanji' => character,
        'codepoint' => codepoint,
        'ids' => ids,
        'dependencies' => [],
        'dependents' => [],
        'traversal' => []
      }
    end
  end

  entries
end

def parse_ids(entries)
  entries.each do |key, value|
    entries[key]['dependencies'] = value['ids'].chars.filter_map do |character|
      next if character == key
      next unless entries.key?(character)
      {
        'kanji' => character,
        'dependencies' => []
      }
    end
  end
end

def populate_all_dependencies(entries)
  entries.each_key do |key|
    entries[key]['dependencies'] = get_dependencies(key, entries)
  end
end

def get_dependencies(kanji, entries)
  entries[kanji]['dependencies'].filter_map do |dependency|
    character = dependency['kanji']
    next unless entries.include?(character)
    {
      'kanji' => character,
      'dependencies' => get_dependencies(character, entries)
    }
  end
end

def traverse_all_dependencies(entries)
  entries.each do |key, value|
    traversal = traverse_dependencies(value['dependencies'])
    entries[key]['traversal'] = traversal
  end
end

def traverse_dependencies(dependencies)
  traversal = []

  dependencies.each do |dependency|
    traverse_dependencies(dependency['dependencies'])
      .each { |d| traversal << d }
    traversal << dependency['kanji']
  end

  traversal.uniq
end

def populate_all_dependents(entries)
  entries.each do |key, value|
    value['traversal'].each do |dependency|
      entry = entries[dependency]
      next if entry['dependents'].include?(key)
      entry['dependents'] << key
    end
  end
end

def save_entries(entries)
  entries.each_value do |entry|
    File.open("data/kanji/#{entry['kanji']}.json", 'w') do |f|
      f.puts entry.to_json
    end
  end
end

update
